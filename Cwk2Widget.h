#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <GL/glu.h>
#include <GL/gl.h>
#include <string>
#include <QPushButton>
using namespace std;

struct materialStruct;

class Cwk2Widget: public QGLWidget
	{ //

	Q_OBJECT

	public:
	Cwk2Widget(QWidget *parent);

	public slots:
  // slots connected to the main window
	void updateRadius(int);
	void updateAngle();
	void roomView(bool);
	void bathView(bool);
	void picView(bool);
	void togglePic(bool);
	void moveDuck(bool);
	void picSize(QString);

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	private:
	void fillImage(string);
	void crossProd(float, float, float, float, float, float);

	// shape and objects for the scene
	void ellipse(float, float, float, const materialStruct*);
	void cuboid(const materialStruct*);
	void _3D_ellipse(float, float, float, const materialStruct*, bool);
	void cylinder(const materialStruct*);
	void pentagon();
	void room();
	void bath();
	void rubberDuck();
	void wallPicture();
	void table();
	void plant();

	// global variables to recieve inputs from the user
	int _w;
	int _h;
	double _angle;
	bool _bathBool;
	bool _roomBool;
	bool _picBool;
	bool _duckBool;
	bool _toggleBool;
	float _radius;
	float normal[3];
	float _picSize;

	}; // class Cwk2Widget

#endif
