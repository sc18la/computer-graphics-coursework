
#include "Cwk2Window.h"

// constructor / destructor
Cwk2Window::Cwk2Window(QWidget *parent)
	: QWidget(parent)
	{ // constructor

	// create menu bar
	menuBar = new QMenuBar(this);

	// create file menu
	fileMenu = menuBar->addMenu("&File");

	// create the action
	actionQuit = new QAction("&Quit", this);

	// add the item to the menu
	fileMenu->addAction(actionQuit);

	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

	// create main widget
  cubeWidget = new Cwk2Widget(this);
  windowLayout->addWidget(cubeWidget);

	// create slider to determine water height
	nVerticesSlider = new QSlider(Qt::Horizontal);
	windowLayout->addWidget(nVerticesSlider);

	// create button and text input menu
	box = new QWidget();
	layout = new QGridLayout();
	box->setMaximumSize(800, 70);
	box->setLayout(layout);

	roomView = new QPushButton("Entire Room View");
	bathView = new QPushButton("Bathtub View");
	picView = new QPushButton("Wall Picture View");
	togglePic = new QPushButton("Change Picture");
	moveDuck = new QPushButton("Move Duck");
	// change the last 2 buttons to toggle buttons
	togglePic->setCheckable(true);
	moveDuck->setCheckable(true);

	textLabel = new QLabel("Picture Size:");
	textLabel->setAlignment(Qt::AlignRight);
	textInput = new QLineEdit();
	textInput->setMaximumSize(150, 20);
	textInput->setPlaceholderText("0-10");
	layout->addWidget(bathView, 0, 1);
	layout->addWidget(picView,  0, 2);
	layout->addWidget(roomView, 0, 3);
	layout->addWidget(togglePic, 0, 4);
	layout->addWidget(moveDuck, 0, 5);
	layout->addWidget(textLabel, 1, 1);
	layout->addWidget(textInput, 1, 2);
	windowLayout->addWidget(box);

	// timer is set up
	ptimer = new QTimer(this);
	ptimer->start(0);

	// connections for interactions
  connect(nVerticesSlider, SIGNAL(valueChanged(int)),  cubeWidget, SLOT(updateRadius(int)));
	connect(roomView, SIGNAL(clicked(bool )), cubeWidget, SLOT(roomView(bool)));
	connect(bathView, SIGNAL(clicked(bool)), cubeWidget, SLOT(bathView(bool)));
	connect(picView, SIGNAL(clicked(bool)), cubeWidget, SLOT(picView(bool)));
	connect(togglePic, SIGNAL(toggled(bool)), cubeWidget, SLOT(togglePic(bool)));
	connect(moveDuck, SIGNAL(toggled(bool)), cubeWidget, SLOT(moveDuck(bool)));
	connect(textInput, SIGNAL(textChanged(const QString)), cubeWidget, SLOT(picSize(const QString)));
	connect(ptimer, SIGNAL(timeout()),  cubeWidget, SLOT(updateAngle()));

	} // constructor

Cwk2Window::~Cwk2Window()
	{ // destructor
	delete ptimer;
	delete nVerticesSlider;
	delete bathView;
	delete picView;
	delete roomView;
	delete togglePic;
	delete moveDuck;
	delete textLabel;
	delete textInput;
	delete layout;
	delete box;
	delete windowLayout;
	delete cubeWidget;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
	} // destructor

// resets all the interface elements
void Cwk2Window::ResetInterface()
	{ // ResetInterface()
	nVerticesSlider->setMinimum(1);
	nVerticesSlider->setMaximum(10);
	roomView->setAutoDefault(true);
	picView->setAutoDefault(true);
	bathView->setAutoDefault(true);
	togglePic->setChecked(false);
	moveDuck->setChecked(false);
	// force refresh
	cubeWidget->update();
	update();
	} // ResetInterface()
