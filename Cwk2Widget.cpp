#include <GL/glu.h>
#include <QGLWidget>
#include <cmath>
#include <string>
#include <iostream>
#include <QImage>
#include "Cwk2Widget.h"
#include "Image.h"
using namespace std;

// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;

// Materials used in the scene are declared below
static materialStruct chromeMaterials = {
  { 0.25, 0.25, 0.25, 1.0},
  { 0.4, 0.4, 0.4, 1.0},
  { 0.774597,0.774597, 0.774597, 1.0},
  76.8
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  50.0
};

static materialStruct redShinyMaterials = {
  { 0.1745, 0.01175, 0.01175, 0.55},
  { 0.61424, 0.04136, 0.04136, 0.55},
  { 0.727811, 0.626959, 0.626959, 0.55},
  76.8
};

static materialStruct yellowPlastic = {
  {0.24725, 0.1995, 0.0745, 1.0 },
	{0.75164, 0.60648, 0.22648, 1.0 },
  {0.628281, 0.555802, 0.366065,  1.0 },
  100.0
};

static materialStruct blackShinyMaterials = {
  {0.05375, 0.05, 0.06625, 0.82 },
	{0.18275, 0.17, 0.22525, 0.82 },
  {0.332741, 0.328634, 0.346435, 0.82},
  38.0
};

static materialStruct greenMaterials = {
  {0.0, 0.0, 0.0, 1.0 },
	{0.1, 0.35, 0.1, 1.0 },
  {0.45, 0.55, 0.45, 1.0},
  32.0
};

static materialStruct brownMaterials = {
  {0.2125, 0.1275, 0.054, 1.0 },
	{0.714, 0.4284, 0.18144, 1.0 },
  {0.393548, 0.271906, 0.166721, 1.0},
  100.0
};

static materialStruct blueMaterials = {
  { 0.1, 0.18725, 0.1745,  1.0},
	{0.396, 0.74151, 0.69102, 1.0},
  {0.297254,0.30829, 0.306678,1.0},
  0.8
};

// global variable pi, regularly used to draw shapes
static double pi = 3.14159265358979323846;

// constructor
Cwk2Widget::Cwk2Widget(QWidget *parent):
  QGLWidget(parent),
  _w(0),
  _h(0),
  _angle(0),
  _bathBool(1),
	_roomBool(1),
	_picBool(1),
	_duckBool(0),
	_toggleBool(0)
  { // constructor
  } // constructor

// called when OpenGL context is set up
void Cwk2Widget::initializeGL()
	{
	// set the widget background colour
	glClearColor(0.660, 0.910, 1, 0.984);
	} // initializeGL()

void Cwk2Widget::updateRadius(int i){
    _radius = i;
    this->repaint();
}
void Cwk2Widget::updateAngle(){
  _angle += 5.0;
  this->repaint();
}
void Cwk2Widget::roomView(bool i){
  _roomBool = i;
  _bathBool = true; _picBool = true;
  this->repaint();
}
void Cwk2Widget::bathView(bool i){
  _bathBool = i;
  _roomBool = true; _picBool = true;
  this->repaint();
}
void Cwk2Widget::picView(bool i){
  _picBool = i;
  _roomBool = true; _bathBool = true;
  this->repaint();
}
void Cwk2Widget::togglePic(bool i){
  _toggleBool = i;
  this->repaint();
}
void Cwk2Widget::moveDuck(bool i){
  _duckBool = i;
  this->repaint();
}
void Cwk2Widget::picSize(QString i){
  // only allows numbers from 0 to 10
  if(i.toFloat()>=0 && i.toFloat()<=10){
    _picSize = i.toFloat();
    this->repaint();
  }
}

// called every time the widget is resized
void Cwk2Widget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

	glMatrixMode(GL_MODELVIEW);
  glEnable(GL_LIGHTING); // enable lighting in general
	glLoadIdentity();
  GLfloat light_pos1[] = {0.0, 10.0 ,10.0, 1.0};
  glEnable(GL_LIGHT0);   // each light source must also be enabled
  glLightfv(GL_LIGHT0, GL_POSITION, light_pos1);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
  glOrtho(-12.0, 10.0, 0.0, 12.0, -30.0, 30.0);

	} // resizeGL()

// creates and fills textures
void Cwk2Widget::fillImage(string imageName){
  glEnable(GL_TEXTURE_2D);
  // 3 textures are generated
  unsigned int texture_wall, texture_marc, texture_map;
  glGenTextures(1, &texture_wall);
  glGenTextures(1, &texture_marc);
  glGenTextures(1, &texture_map);
  // 3 images in the directory are loaded and saved to separate textures
  string Tiles = "Tiles2.ppm", Marc = "Marc_Dekamps.ppm", Map = "Mercator-projection.ppm";
  QImage* loadedImage;
  unsigned int _width = Image(imageName).Width();
  unsigned int _height = Image(imageName).Height();
  unsigned int N_COLOUR = 3;
  if(imageName.compare(Tiles) ==0){
    loadedImage = new QImage("Tiles2.ppm");
    GLubyte wall[_width][_height][N_COLOUR];
    for(unsigned int i_x_pix = 0; i_x_pix < _width; i_x_pix++)
      for(unsigned int i_y_pix = 0; i_y_pix < _height; i_y_pix++){
        QRgb colval = loadedImage->pixel(i_x_pix,i_y_pix);
        wall[_height - 1 - i_y_pix][i_x_pix][0] = qRed(colval);
        wall[_height - 1 - i_y_pix][i_x_pix][1] = qGreen(colval);
        wall[_height - 1 - i_y_pix][i_x_pix][2] = qBlue(colval);
    }
    //Each image is bound to its corresponding texture
    glBindTexture(GL_TEXTURE_2D, texture_wall);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, _width, _height, 0,GL_RGB, GL_UNSIGNED_BYTE, wall);
  }
  else if(imageName.compare(Marc) ==0){
    loadedImage = new QImage("Marc_Dekamps.ppm");
    GLubyte marc[_width][_height][N_COLOUR];
    for(unsigned int i_x_pix = 0; i_x_pix < _width; i_x_pix++)
      for(unsigned int i_y_pix = 0; i_y_pix < _height; i_y_pix++){
        QRgb colval = loadedImage->pixel(i_x_pix,i_y_pix);
        marc[_height - 1 - i_y_pix][i_x_pix][0] = qRed(colval);
        marc[_height - 1 - i_y_pix][i_x_pix][1] = qGreen(colval);
        marc[_height - 1 - i_y_pix][i_x_pix][2] = qBlue(colval);
    }
    glBindTexture(GL_TEXTURE_2D, texture_marc);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, _width, _height, 0,GL_RGB, GL_UNSIGNED_BYTE, marc);
  }
  else if(imageName.compare(Map) ==0){
    loadedImage = new QImage("Mercator-projection.ppm");
    GLubyte map[_width][_height][N_COLOUR];
    for(unsigned int i_x_pix = 0; i_x_pix < _width; i_x_pix++)
      for(unsigned int i_y_pix = 0; i_y_pix < _height; i_y_pix++){
        QRgb colval = loadedImage->pixel(i_x_pix,i_y_pix);
        map[_height - 1 - i_y_pix][i_x_pix][0] = qRed(colval);
        map[_height - 1 - i_y_pix][i_x_pix][1] = qGreen(colval);
        map[_height - 1 - i_y_pix][i_x_pix][2] = qBlue(colval);
    }
    glBindTexture(GL_TEXTURE_2D, texture_map);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, _width, _height, 0,GL_RGB, GL_UNSIGNED_BYTE, map);
  }
}

// this functions finds the normal of a plane using 2 vectors
void Cwk2Widget::crossProd(float a1, float a2, float a3,
                           float b1, float b2, float b3){
  normal[0] = a2*b3 - a3*b2;
  normal[1] = a3*b1 - a1*b3;
  normal[2] = a1*b2 - a2*b1;
}

void Cwk2Widget::room(){
  // the walls and floors of the room are made up from a white shiny material
  materialStruct* p_front = &whiteShinyMaterials;
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  glActiveTexture(GL_TEXTURE0);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  fillImage("Tiles2.ppm");

  /*Walls*/
  glBegin(GL_QUADS);
  crossProd(0,0,1,0,-1,0);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,0,-12.5);
  glMultiTexCoord2f(GL_TEXTURE0,0,0);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,15,-12.5);
  glMultiTexCoord2f(GL_TEXTURE0,3,0);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,15,8.5);
  glMultiTexCoord2f(GL_TEXTURE0,3,3);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,0,8.5);
  glMultiTexCoord2f(GL_TEXTURE0,0,3);
  glEnd();

  glBegin(GL_QUADS);
  crossProd(1,0,0,0,1,0);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,0,-12.5);
  glMultiTexCoord2f(GL_TEXTURE0,0,0);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,15,-12.5);
  glMultiTexCoord2f(GL_TEXTURE0,3,0);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(13,15,-12.5);
  glMultiTexCoord2f(GL_TEXTURE0,3,3);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(13,0,-12.5);
  glMultiTexCoord2f(GL_TEXTURE0,0,3);
  glEnd();

  // this wall is removed when the user is viewing the picture on the wall
  if(_picBool){
    glBegin(GL_QUADS);
    crossProd(0,0,1,0,1,0);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(13,0,-12.5);
    glMultiTexCoord2f(GL_TEXTURE0,0,0);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(13,15,-12.5);
    glMultiTexCoord2f(GL_TEXTURE0,3,0);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(13,15,8.5);
    glMultiTexCoord2f(GL_TEXTURE0,3,3);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(13,0,8.5);
    glMultiTexCoord2f(GL_TEXTURE0,0,3);
    glEnd();
  }

  /*Floor*/
  glBegin(GL_QUADS);
  crossProd(-1,0,0,0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,0,-12.5);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f( 13,0,-12.5);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f( 13,0, 8.5);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-12,0,8.5);
  glEnd();
  glBindTexture(GL_TEXTURE_2D, 0);
}

// creates a 1x1 cuboid with a given material
void Cwk2Widget::cuboid(const materialStruct* p_front){
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  //Sides
  //Back
  glBegin(GL_QUADS);
  crossProd(0,1,0,1,0,0);
  glNormal3f(normal[0], normal[1], normal[2]);glVertex3f( 0,0,0);
  glNormal3f(normal[0], normal[1], normal[2]);glVertex3f( 1,0,0);
  glNormal3f(normal[0], normal[1], normal[2]);glVertex3f( 1,1,0);
  glNormal3f(normal[0], normal[1], normal[2]);glVertex3f( 0,1,0);
  glEnd();
  //Left
  glBegin(GL_QUADS);
  crossProd(0,1,0,0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,0,0);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,1,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,1,0);
  glEnd();
  //Front
  glBegin(GL_QUADS);
  crossProd(0,-1,0,1,0,0);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,1,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,1,1);
  glEnd();
  //Right
  glBegin(GL_QUADS);
  crossProd(0,1,0,0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,1,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,1,0);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,0,0);
  glEnd();
  //Top
  glBegin(GL_QUADS);
  crossProd(-1,0,0,0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,1,0);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,1,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,1,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,1,0);
  glEnd();
  //Bottom
  glBegin(GL_QUADS);
  crossProd(-1,0,0,0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,0,0);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 0,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,0,1);
  glNormal3f(normal[0], normal[1], normal[2]); glVertex3f( 1,0,0);
  glEnd();
}

// draws a sphere or semi-sphere that can be skewed, with a given material
void Cwk2Widget::_3D_ellipse(float x_length, float y_length, float z_length,
                              const materialStruct* p_front, bool semi){
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  float phi_min, phi_max, theta_min, theta_max;
  // if a semi-sphere is chosen to be drawn, the angles' ranges will be limited
  if(semi){
    phi_min = 0; phi_max = pi;
    theta_min = -pi; theta_max = 0;}

  else{
    phi_min = 0; phi_max = 2*pi;
    theta_min = -pi; theta_max = pi;}

  int n_theta = 100;
  int n_phi   = 100;

  float delta_phi   = (phi_max - phi_min)/n_phi;
  float delta_theta = (theta_max - theta_min)/n_theta;

  for (int i_phi = 0; i_phi < n_phi; i_phi++)
    for (int i_theta = 0; i_theta < n_theta; i_theta++){
      glBegin(GL_POLYGON);
      float phi   = phi_min + i_phi*delta_phi;
      float theta = theta_min + i_theta*delta_theta;
      float x_0 = x_length*cos(phi)*sin(theta);
      float y_0 = y_length*sin(phi)*sin(theta);
      float z_0 = z_length*cos(theta);
      glNormal3f(x_0,y_0,z_0);
      glVertex3f(x_0,y_0,z_0);
      float x_1 = x_length*cos(phi+delta_phi)*sin(theta);
      float y_1 = y_length*sin(phi+delta_phi)*sin(theta);
      float z_1 = z_length*cos(theta);
      glNormal3f(x_1,y_1,z_1);
      glVertex3f(x_1,y_1,z_1);
      float x_2 = x_length*cos(phi+delta_phi)*sin(theta+delta_theta);
      float y_2 = y_length*sin(phi+delta_phi)*sin(theta+delta_phi);
      float z_2 = z_length*cos(theta + delta_theta);
      glNormal3f(x_2,y_2,z_2);
      glVertex3f(x_2,y_2,z_2);
      float x_3 = x_length*cos(phi)*sin(theta+delta_theta);
      float y_3 = y_length*sin(phi)*sin(theta+delta_phi);
      float z_3 = z_length*cos(theta + delta_theta);
      glNormal3f(x_3,y_3,z_3);
      glVertex3f(x_3,y_3,z_3);
      glEnd();
  }
}

// draws a 2D ellipse with a hole in the middle, with a given material
void Cwk2Widget::ellipse(float x_length, float z_length,
                         float thick, const materialStruct* p_front){
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  // it is drawn in the x and z direction
  float x0, z0, x1, z1;
  float theta_min = 0;
  float theta_max = 2*pi;
  int n_theta = 100;
  float delta_theta = (theta_max - theta_min)/n_theta;

  for (int i_theta = 0; i_theta < n_theta; i_theta++){
    x0 = cos(i_theta*delta_theta)*x_length;
    z0 = sin(i_theta*delta_theta)*z_length;
    x1 = cos(i_theta*delta_theta+delta_theta)*x_length;
    z1 = sin(i_theta*delta_theta+delta_theta)*z_length;
    glBegin(GL_QUADS);
    crossProd(1,0,0,0,0,-1);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(x0,0,z0);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(x0*thick,0,z0*thick);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(x1*thick,0,z1*thick);
    glNormal3f(normal[0], normal[1], normal[2]);
    glVertex3f(x1,0,z1);
    glEnd();
  }
}

// draws a cylinder with a given material
void Cwk2Widget::cylinder(const materialStruct* p_front){
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  int N = 100;
  int n_div  =  100;

  float x0, x1, z0, z1;
  float y_min = -1;
  float y_max =  1;
  float delta_y = (y_max - y_min)/n_div;

  for (int i = 0; i < N; i++){
    for(int i_y = 0; i_y < n_div; i_y++){
      x0 = cos(2*i*pi/N);
      x1 = cos(2*(i+1)*pi/N);
      z0 = sin(2*i*pi/N);
      z1 = sin(2*(i+1)*pi/N);

      float y = y_min + i_y*delta_y;
      glBegin(GL_POLYGON);
      glVertex3f(x0,y,z0);
      glNormal3f(x0,0,z0);
      glVertex3f(x1,y,z1);
      glNormal3f(x1,0,z1);
      glVertex3f(x1,y+delta_y,z1);
      glNormal3f(x1,0,z1);
      glVertex3f(x0,y+delta_y,z0);
      glNormal3f(x0,0,z0);
      glEnd();
    }
  }
}

// draws a pentagon and fills it with a texture
void Cwk2Widget::pentagon(){
  glActiveTexture(GL_TEXTURE1);
  materialStruct* p_front = &whiteShinyMaterials;
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  // default texture is Marc's face
  if(!_toggleBool){fillImage("Marc_Dekamps.ppm");}
  // chages the texture to the map image if the button is toggled
  if(_toggleBool){fillImage("Mercator-projection.ppm");}

  glBegin(GL_POLYGON);
  glColor3f(1.0f, 1.0f, 1.0f);
  crossProd(1,0,0,0,1,0);
  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f( 0,0,0);
  glMultiTexCoord2f(GL_TEXTURE1,0,0.667);

  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(-(sin(18*pi/180)/sin(90*pi/180)), sin(72*pi/180)/sin(90*pi/180), 0);
  glMultiTexCoord2f(GL_TEXTURE1,0.5, 1);

  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(0.5, 0.5*sin(72*pi/180)/sin(18*pi/180), 0);
  glMultiTexCoord2f(GL_TEXTURE1,1.0, 0.667);

  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(1+(sin(18*pi/180)/sin(90*pi/180)), sin(72*pi/180)/sin(90*pi/180), 0);
  glMultiTexCoord2f(GL_TEXTURE1,0.75,0);

  glNormal3f(normal[0], normal[1], normal[2]);
  glVertex3f(1,0,0);
  glMultiTexCoord2f(GL_TEXTURE1,0.25,0);
  glEnd();
  glBindTexture(GL_TEXTURE_2D, 0);
}

// draws the picture frame in black and puts image in the frame
void Cwk2Widget::wallPicture(){
  glPushMatrix();glPushMatrix();glPushMatrix();glPushMatrix();glPushMatrix();
  glPushMatrix();
  glScalef(3.0, 3.0, 3.0);
  this->pentagon();
  glPopMatrix();
  glTranslatef(-0.25, -0.1, 0.0);
  glRotatef(18, 0.0, 0.0, 1.0);
  glScalef(0.3, 3.3, 0.3);
  this->cuboid(&blackShinyMaterials);
  glPopMatrix();
  glTranslatef(-0.2-3*(sin(18*pi/180)/sin(90*pi/180)), 0.2+3*sin(72*pi/180)/sin(90*pi/180), 0.0);
  glRotatef(-54, 0.0, 0.0, 1.0);
  glScalef(0.3, 3.3, 0.3);
  this->cuboid(&blackShinyMaterials);
  glPopMatrix();
  glTranslatef(3*(1+(sin(18*pi/180)/sin(90*pi/180))), 3*sin(72*pi/180)/sin(90*pi/180), 0);
  glRotatef(54, 0.0, 0.0, 1.0);
  glScalef(0.3, 3.3, 0.3);
  this->cuboid(&blackShinyMaterials);
  glPopMatrix();
  glTranslatef(3, 0, 0);
  glRotatef(-18, 0.0, 0.0, 1.0);
  glScalef(0.3, 3.3, 0.3);
  this->cuboid(&blackShinyMaterials);
  glPopMatrix();
  glTranslatef(-0.3, 0.0, 0.0);
  glRotatef(-90, 0.0, 0.0, 1.0);
  glScalef(0.3, 3.6, 0.3);
  this->cuboid(&blackShinyMaterials);
  glPopMatrix();
  glTranslatef(1.5, 0.1+1.5*sin(72*pi/180)/sin(18*pi/180), 0.1);
  this->_3D_ellipse(0.4, 0.4, 0.4, &blackShinyMaterials, false);
  glBindTexture(GL_TEXTURE_2D, 0);
}

//draws a bath
void Cwk2Widget::bath(){
  //Bath Tub
  this->_3D_ellipse(6.0, 2.0, 3.0, &whiteShinyMaterials, true);
  this->_3D_ellipse(5.5, 1.5, 2.5, &whiteShinyMaterials, true);
  this->ellipse(5.0, 2.5, 1.2, &whiteShinyMaterials);
  glPushMatrix();glPushMatrix();glPushMatrix();glPushMatrix();glPushMatrix();
  //Water
  glTranslatef(0.0,-1.0+ _radius/100, 0.0);
  this->ellipse(_radius/18, _radius/36, 0.0, &blueMaterials);
  glPopMatrix();
  glTranslatef(4.0, 0.0, 0.0);
  glScalef(0.2, 1.0, 0.2);
  this->cylinder(&blueMaterials);
  //Bath Tap
  glPopMatrix();
  glTranslatef(5.6, -2.5, 0.0);
  glScalef(0.5, 4.0, 0.5);
  this->cuboid(&chromeMaterials);
  glPopMatrix();
  glTranslatef(3.75, 1.0, 0.0);
  glScalef(2.0, 0.5, 0.5);
  this->cuboid(&chromeMaterials);
  //Bath Feet
  glPopMatrix();
  glRotatef(-30, 0.0, 0.0, 1.0);
  glScalef(0.5, 1.1, 4.0);
  glTranslatef(-3.0, -3.1, -0.45);
  this->cuboid(&chromeMaterials);
  glPopMatrix();
  glRotatef(30, 0.0, 0.0, 1.0);
  glScalef(0.5, 1.1, 4.0);
  glTranslatef(3.0, -3.4, -0.45);
  this->cuboid(&chromeMaterials);
}

// draws a rubber duck
void Cwk2Widget::rubberDuck(){
  glPushMatrix();
  //Body
  this->_3D_ellipse(4.0, 2.0, 2.0, &yellowPlastic, false);
  glTranslatef(-1.7, 0.6, 0.0);
  glRotatef(-30, 0.0, 0.0, 1.0);
  this->_3D_ellipse(4.0, 2.0, 2.0, &yellowPlastic, true);
  this->_3D_ellipse(4.0, 0.0, 2.0, &yellowPlastic, true);
  //Head
  glPopMatrix();
  glTranslatef(2.0, 3.3, 0.0);
  this->_3D_ellipse(2.0, 3.0, 2.0, &yellowPlastic, false);
  glPushMatrix();
  //Beak
  glTranslatef(1.5, -0.1, 0.0);
  this->_3D_ellipse(1.3, 0.4, 1.3, &redShinyMaterials, false);
  glTranslatef(0.0, -0.4+sin(_angle)/4.0, 0.0);
  this->_3D_ellipse(1.3, 0.4, 1.3, &redShinyMaterials, false);
  //Eyes
  glPopMatrix();
  glTranslatef(1.0, 1.3, -0.6);
  glPushMatrix();
  this->_3D_ellipse(1.0, 1.0, 0.5, &whiteShinyMaterials, false);
  glTranslatef(0.9, 0.4, 0.0);
  this->_3D_ellipse(0.2, 0.3, 0.2, &blackShinyMaterials, false);
  glPopMatrix();
  glTranslatef(0.0, 0.0, 1.2);
  this->_3D_ellipse(1.0, 1.0, 0.5, &whiteShinyMaterials, false);
  glTranslatef(0.9, 0.4, 0.0);
  this->_3D_ellipse(0.2, 0.3, 0.2, &blackShinyMaterials, false);
}

// draws a table from a cuboid and cylinders
void Cwk2Widget::table(){
  glPushMatrix();glPushMatrix();glPushMatrix();glPushMatrix();
  glTranslatef(0.0, 3.0, 0.0);
  glScalef(5.0, 0.5, 10.0);
  this->cuboid(&blackShinyMaterials);
  glPopMatrix();
  glTranslatef(0.2, 0.0, 9.7);
  glScalef(0.2, 3.0, 0.2);
  this->cylinder(&chromeMaterials);
  glPopMatrix();
  glTranslatef(0.2, 0.0, 0.2);
  glScalef(0.2, 3.0, 0.2);
  this->cylinder(&chromeMaterials);
  glPopMatrix();
  glTranslatef(4.5, 0.0, 0.2);
  glScalef(0.2, 3.0, 0.2);
  this->cylinder(&chromeMaterials);
  glPopMatrix();
  glTranslatef(4.5, 0.0, 9.7);
  glScalef(0.2, 3.0, 0.2);
  this->cylinder(&chromeMaterials);
}

// draws a plant with a pot and 2D leaves
void Cwk2Widget::plant(){
  glPushMatrix();glPushMatrix();glPushMatrix();glPushMatrix();
  glScalef(1.5, 1.3, 1.5);
  this->cylinder(&chromeMaterials);
  glPopMatrix();
  glScalef(1.1, 1.3, 1.1);
  this->cylinder(&chromeMaterials);
  glPopMatrix();
  glTranslatef(0.0, 1.3, 0.0);
  this->ellipse(1.1, 1.1, 1.39, &chromeMaterials);
  glPopMatrix();
  glScalef(0.15, 6.0, 0.15);
  this->cylinder(&brownMaterials);
  glPopMatrix();
  glTranslatef(0.0, 6.0, 0.0);
  // 15 leaves are drawn and rotates to make the plant look realistic
  for(int i = 0; i<15; i++){
    glRotatef(i*3, 1.0, 1.0, 1.0);
    this->ellipse(2.5, 0.2, 0.0, &greenMaterials);
  }
}
// called every time the widget needs painting
void Cwk2Widget::paintGL()
{ // paintGL()
	// clear the widget
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glEnable(GL_NORMALIZE);
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_SMOOTH);

	glLoadIdentity();
  gluLookAt(0.3,0.15,1., 0.0,0.0,0.0, 0.0,1.0,0.0);
  //room view
  if(!_roomBool){
    glLoadIdentity();
    gluLookAt(0.3,0.15,1., 0.0,0.0,0.0, 0.0,1.0,0.0);
  }
  //bath view
  if(!_bathBool){
    glLoadIdentity();
    glScalef(1.5, 1.5, 1.5);
    glTranslatef(-5.0, -2.0, 0.0);
    gluLookAt(-0.2,0.3,1., 0.0,0.0,0.0, 0.0,1.0,0.0);
  }
  //picture view
  if(!_picBool){
    glLoadIdentity();
    glTranslatef(-2.5,-2.0,20.0);
    gluLookAt(10.0, 0.1,1.0,  0.0,0.0,0.0, 0.0,1.0,0.0);
  }

  // place all objects on the screen and transform as necessary
  this->room();
  glPushMatrix();glPushMatrix();glPushMatrix();
  glTranslatef(5.5,2.5,-7.5);
  glPushMatrix();
  glScalef(1.0, 1.0, 1.5);
  this->bath();
  glPopMatrix();
  glTranslatef(-_radius/30, -0.9+ _radius/100, 0.0);
  glScalef(0.15,0.15,0.15);
  glPushMatrix();
  glRotatef(-90, 0.0, 1.0, 0.0);
  glPopMatrix();
  // moves the duck in an oval path when the button is toggled
  if(_duckBool){
    glTranslatef(sin(_angle*pi/180)*_radius/6, 0.0, cos(_angle*pi/180)*_radius/6);
    glTranslatef(_radius*6/30, 0.0, 0.0);
    glRotatef(_angle, 0.0, 1.0, 0.0);
  }
  this->rubberDuck();
  glPopMatrix();
  glTranslatef(-12.0, 0.0, -12.0);
  glScalef(1.1, 1.0, 2.0);
  this->table();
  glPopMatrix();
  glTranslatef(-3.5, 0.0, -10.0);
  glScalef(0.9, 0.9, 0.9);
  this->plant();
  glPopMatrix();
  glRotatef(90, 0.0, 1.0, 0.0);
  glTranslatef(0.0, 5.0, -11.999);
  glScalef(1.0+_picSize/10, 1.0+_picSize/10, 1.0+_picSize/10);
  this->wallPicture();

	// flush to screen
	glFlush();

} // paintGL()
