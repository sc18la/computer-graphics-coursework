#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QTimer>
#include <QBoxLayout>
#include <QGroupBox>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <string>
#include "Cwk2Widget.h"
using namespace std;

class Cwk2Window: public QWidget
	{
	public:
	// constructor / destructor
	Cwk2Window(QWidget *parent);
	~Cwk2Window();

	signals:
	void valueChanged(int j);

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;

	// beneath that, the main widget
	Cwk2Widget *cubeWidget;
	// and a slider for the number of vertices
	QSlider *nVerticesSlider;
	// add buttons
	QWidget *box;
	QGridLayout *layout;
	QPushButton *bathView;
	QPushButton *picView;
	QPushButton *roomView;
	QPushButton *togglePic;
	QPushButton *moveDuck;
	QLabel *textLabel;
	QLineEdit *textInput;

	// a timer
	QTimer *ptimer;

	// resets all the interface elements
	void ResetInterface();
	};

#endif
