To Run:
Type the following commands into the terminal:
1. module add qt
2. qmake Cwk2.pro
3. make
4. ./Cwk2

How To Interact:
-Move the slider across horizontally to fill the bathtub
-Click the buttons to change the camera view
-Click the toggle buttons to move the rubber duck and to change the picture
-The window is resizeable
